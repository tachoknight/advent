//
//  ConditionalsSection.cpp
//  Advent
//
//  Created by Ron Olson on 9/22/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//

#include "ConditionalsSection.h"

void ConditionalsSection::addLine(int num, const vector<string>& lineTokens)
{
    // Ultimately we want this as a vector of ints, not strings, and bypass
    // the first element, soooo...
    vector<int> items;

    // beginning at the second element, copy the lineTokens vector into the items vector, converting along the way...
    transform(lineTokens.begin() + 1, lineTokens.end(), back_inserter(items), [] (const string& token)
    {
        return stoi(token);
    });

    // Search for the line
    auto it = num2Locations.find(num);
    // find it?
    if (it == num2Locations.end())
    {
        // no, so add it now
        num2Locations.insert(std::make_pair(num, items));
    }
    else
    {
        it->second.insert(it->second.end(), items.begin(), items.end());
    }
}

void ConditionalsSection::dumpDB()
{
    cout << "Dumping DB for ConditionalsSection" << endl;

    for_each(num2Locations.begin(), num2Locations.end(),
             [](std::map<int, vector<int>>::value_type& kv)
    {
        cout << "Key: " << kv.first;

        for_each(kv.second.begin(), kv.second.end(), [](std::vector<int>::value_type& num)
        {
            cout << " " << num ;
        });

        cout << endl;
    });


    cout << "Finished dumping DB for ConditionalsSection" << endl;
}