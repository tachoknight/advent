//
//  MainDescriptionSection.h
//  Advent
//
//  Created by Ron Olson on 9/16/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//

#ifndef __Advent__MainDescriptionSection__
#define __Advent__MainDescriptionSection__

#include "Section.h"

#include <map>
#include <string>
#include <vector>
#include <iostream>
#include <sstream>
using namespace std;

//C  SECTION 1: LONG FORM DESCRIPTIONS.  EACH LINE CONTAINS A LOCATION NUMBER,
//C	A TAB, AND A LINE OF TEXT.  THE SET OF (NECESSARILY ADJACENT) LINES
//C	WHOSE NUMBERS ARE X FORM THE LONG DESCRIPTION OF LOCATION X.

class MainDescriptionSection : public Section
{
    map<int, string> num2Desc;

public:
    MainDescriptionSection() = default;

    virtual void addLine(int num, const vector<string>& lineTokens);
    virtual void dumpDB();
    virtual unsigned long getDBSize()
    {
        return num2Desc.size();
    }

    // Here begins the methods specific to this particular class
    string getDescription(int id);
};


#endif /* defined(__Advent__MainDescriptionSection__) */
