//
//  Utility.h
//  Advent
//
//  Created by Ron Olson on 9/16/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//

#ifndef __Advent__Utility__
#define __Advent__Utility__

#include <algorithm>
#include <vector>
#include <string>
using namespace std;

//
// Helper functions; because this project is to exercise knowledge of a language, and not
// 3rd-party libraries, I'm not using Boost or other good things like that so that it keeps
// the code "pure" (in air-quotes a thousand feet tall) and can be used without any dependencies
//
vector<string> split(const string& s, char delim);
string trim(const std::string& str);


#endif /* defined(__Advent__Utility__) */
