#include "ObjectDescription.h"
#include "ObjectSection.h"
#include <cassert>

using namespace std;


void ObjectSection::addLine(int num, const vector<string>& lineTokens)
{
    /*
    1
    1       SET     OF      KEYS
    0
    000     THERE   ARE     SOME    KEYS    ON      THE     GROUND  HERE.
    2
    2       BRASS   LANTERN
    0
    000     THERE   IS      A       SHINY   BRASS   LAMP    NEARBY.
    100
    100     THERE   IS      A       LAMP    SHINING NEARBY.
    */

    // This section deals with objects, which have multiple states, which all have to be handled.
    // To do this, we have a separate class, ObjectDescription, which will hold both the main description,
    // as well as the various sub-descriptions (which may be split over multiple lines). What we need to
    // do here is maintain the map of id to the object

    // Search for the line, but here we have to use the first element in the vector, not the
    // num parameter, because we may be on "sub-lines" that will have the same ID over and over
    // again (e.g. 000, 100, etc.)

    auto id = atoi(lineTokens[0].c_str());

    // Okay, we know that there is no object 0, so if id is in fact 0, that means it's the first sub-description,
    // But we also know that if the number is < 100, that's the beginning of a new object
    if (id < 100 && id != 0)
    {
        // This is an object ID, so that means we have to set up a new ObjectDescription object
        this->currentID = id;

		auto od = make_shared<ObjectDescription>(ObjectDescription());
        od->setMainDescription(assembleLine(lineTokens));

        // And now add it to the map
        num2Description.insert(std::make_pair(id, od));
    }
    else
    {
        // If we're here, that means that we have a sub description, whether it be 0
        // or 100 on up, but we know this is part of an object we've already handled and
        // have set via this->currentID.

        // So we need to get that object ...
        auto it = num2Description.find(this->currentID);
        assert(it != num2Description.end());

        // ... and add the sub part
        it->second->setSubDescriptionLine(id, assembleLine(lineTokens));
    }
}

void ObjectSection::dumpDB()
{
    cout << "Dumping DB for ObjectSection" << endl;

    for_each(num2Description.begin(), num2Description.end(),
             [](std::map<int, shared_ptr<ObjectDescription>>::value_type& kv)
    {
        cout << "Key: " << kv.first << " - " << kv.second->getMainDescription() << endl;
        cout << "\tSubValueDesc: " << endl;
        kv.second->dumpDB();

        cout << endl;
    });

    cout << "Finished dumping DB for ObjectSection" << endl;
}

shared_ptr<ObjectDescription> ObjectSection::getDescriptionForObject(const int& objNum) const
{
	auto it = num2Description.find(objNum);
	assert(it != num2Description.end());

	return it->second;
}