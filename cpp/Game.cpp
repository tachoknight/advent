//
//  Game.cpp
//  Advent
//
//  Created by Ron Olson on 9/16/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//

#include "Room.h"
#include "Section.h"
#include "MainDescriptionSection.h"
#include "ShortDescriptionSection.h"
#include "TravelSection.h"
#include "VerbsSection.h"
#include "ObjectSection.h"
#include "ArbitraryMessagesSection.h"
#include "ObjectLocationSection.h"
#include "ActionDefaultsSection.h"
#include "ConditionalsSection.h"
#include "ClassMessageSection.h"
#include "HintsSection.h"
#include "MagicMessageSection.h"
#include "Game.h"

// This method is responsible for setting up the game in that it reads from
// the database file (passed in to the constructor) and will build the world
// to play in
void Game::initializeGame(const string& dbFile)
{
    // What it says on the box: read in the database file and
    // build all the "Section" objects from it
    buildWorld(dbFile);
    
    // Now that the world is built, we have to get a little more
    // into the weeds with specifics about how to handle certain
    // things
    buildActionLogic();
    
    // And seed our random number generator for the game
    std::srand(static_cast<unsigned int>(::time(0)));
}

void Game::cleanupGame()
{
    cout << "Cleaning up..." << endl;
    
    // We're going to remove all the objects from our dbSections map
    for (auto it = dbSections.begin(); it != dbSections.end();)
    {
        dbSections.erase(it++);
    }
}

//
// Here we add our various section objects that will hold the database in
// the way its laid out, section by section. These objects will be populated
// from the file and then used to build the world
//
void Game::addSectionObjects()
{
    dbSections.insert(std::make_pair(SECTION_TYPE::MAIN_DESCRIPTIONS, make_shared<MainDescriptionSection>(MainDescriptionSection())));
    dbSections.insert(std::make_pair(SECTION_TYPE::SHORT_DESCRIPTIONS, make_shared<ShortDescriptionSection>(ShortDescriptionSection())));
    dbSections.insert(std::make_pair(SECTION_TYPE::TRAVEL_TABLE, make_shared<TravelSection>(TravelSection())));
    dbSections.insert(std::make_pair(SECTION_TYPE::VOCABULARY, make_shared<VerbsSection>(VerbsSection())));
    dbSections.insert(std::make_pair(SECTION_TYPE::OBJECT_DESCRIPTION, make_shared<ObjectSection>(ObjectSection())));
    dbSections.insert(std::make_pair(SECTION_TYPE::ARBITRARY_MESSAGES, make_shared<ArbitraryMessagesSection>(ArbitraryMessagesSection())));
    dbSections.insert(std::make_pair(SECTION_TYPE::OBJECT_LOCATIONS, make_shared<ObjectLocationSection>(ObjectLocationSection())));
    dbSections.insert(std::make_pair(SECTION_TYPE::ACTION_DEFAULTS, make_shared<ActionDefaultsSection>(ActionDefaultsSection())));
    dbSections.insert(std::make_pair(SECTION_TYPE::ASSETS, make_shared<ConditionalsSection>(ConditionalsSection())));
    dbSections.insert(std::make_pair(SECTION_TYPE::CLASS_MESSAGES, make_shared<ClassMessageSection>(ClassMessageSection())));
    dbSections.insert(std::make_pair(SECTION_TYPE::HINTS, make_shared<HintsSection>(HintsSection())));
    dbSections.insert(std::make_pair(SECTION_TYPE::MAGIC_MESSAGES, make_shared<MagicMessageSection>(MagicMessageSection())));
}

// Returns the appropriate section object for the given number
shared_ptr<Section> Game::getDBSection(const SECTION_TYPE& sectionNum)
{
    auto currentSection = dbSections.find(sectionNum);
    assert(currentSection != dbSections.end());
    
    return currentSection->second;
}

// Returns the appropriate section object cast to the derived class that
// has the data we need plus whatever additional derived-specific methods
// that we need. Calls getDBSection() above so it can do the assertion
// in case we got a bad section
template <class T> shared_ptr<T> Game::getDBSection(const SECTION_TYPE& sectionNum)
{
    return dynamic_pointer_cast<T>(getDBSection(sectionNum));
}


// Given a line from the database, calls on the right object to handle it
void Game::handleLine(const SECTION_TYPE& sectionNum, const vector<string>& lineTokens)
{
    shared_ptr<Section> section = getDBSection(sectionNum);
    assert(section != nullptr);
    
    section->addLine(atoi(lineTokens[0].c_str()),
                     lineTokens);
}

void Game::setUpWorld()
{
    // Room #1 in the database is where the game begins.
    const int FIRST_ROOM = 1;
    
    // Okay, here is where we're going to set up everything for the game.
    // The player starts in room 1 (YOU ARE STANDING AT THE END OF A ROAD...)
    // and it's unconditional (the 0), so we are not going to worry about
    // checking the return because it's supposed to happen no matter what
    setRoom(FIRST_ROOM, 0);
}

bool Game::setRoom(const int& roomNum, const int& motionCondition)
{
    // Before we can set the new room, we have to check the motion
    // condition. In the original Fortran code this is described as:
    //      C	MEANWHILE, M SPECIFIES THE CONDITIONS ON THE MOTION.
    //      C		IF M=0		IT'S UNCONDITIONAL.
    //      C		IF 0<M<100	IT IS DONE WITH M% PROBABILITY.
    //      C		IF M=100	UNCONDITIONAL, BUT FORBIDDEN TO DWARVES.
    //      C		IF 100<M<=200	HE MUST BE CARRYING OBJECT M-100.
    //      C		IF 200<M<=300	MUST BE CARRYING OR IN SAME ROOM AS M-200.
    //      C		IF 300<M<=400	PROP(M MOD 100) MUST *NOT* BE 0.
    //      C		IF 400<M<=500	PROP(M MOD 100) MUST *NOT* BE 1.
    //      C		IF 500<M<=600	PROP(M MOD 100) MUST *NOT* BE 2, ETC.
    //
    // If motionCondition == 0, then it's unconditional and must happen,
    // so we don't have to worry about any additional conditional logic
    
    // What we use to determine whether we can move. This is
    // defaulted to true to handle the presumed motionCondition
    // value of 0, and if it's not, then that will be handled in
    // the next section
    bool okayToMove = true;
    
    if (motionCondition != 0)
    {
        // Okay, so our motion condition is not 0 (unconditional). So, what is it?
        
        if (0 < motionCondition && motionCondition < 100)
        {
            // "IF 0<M<100	IT IS DONE WITH M% PROBABILITY."
            
            okayToMove = (std::rand() % 100) < motionCondition;
        }
        else if (motionCondition == 100)
        {
            // IF M=100	UNCONDITIONAL, BUT FORBIDDEN TO DWARVES.
            // TODO: Dwarf Check
            okayToMove = true;
        }
        else if (100 < motionCondition && motionCondition <= 200)
        {
            // IF 100<M<=200	HE MUST BE CARRYING OBJECT M-100.
            okayToMove = this->currentPlayer.hasObject(motionCondition - 100);
        }
        else if (200 < motionCondition && motionCondition <= 300)
        {
            // IF 200<M<=300	MUST BE CARRYING OR IN SAME ROOM AS M-200.
            if (this->currentPlayer.hasObject(motionCondition - 200) || this->currentRoom == (motionCondition - 200))
                okayToMove = true;
            else
                okayToMove = false;
        }
        else
        {
            //      C		IF 300<M<=400	PROP(M MOD 100) MUST *NOT* BE 0.
            //      C		IF 400<M<=500	PROP(M MOD 100) MUST *NOT* BE 1.
            //      C		IF 500<M<=600	PROP(M MOD 100) MUST *NOT* BE 2, ETC.
            int prop = motionCondition % 100;
        }
    }
    
    // Did we pass the motion condition checks and can move to the new
    // room?
    if (okayToMove == true)
    {
        // Yes we did, so now the player is gonna move...
        
        // Before adding a new room, let's see if the room is already
        // there
        auto it = roomMap.find(roomNum);
    
        if (it == roomMap.end())
        {
            // No, this is a new room, so let's build it now
            // Call our buildRoom method for the room we're supposed to be setting
            // up
            roomMap.insert(std::make_pair(roomNum, buildRoom(roomNum)));
        }
    
        // Regardless of whether we've built a new room or not, set our room
        // number to the room number
        this->currentRoom = roomNum;
    }
    
    // And indicate that yes, we did set the room
    return okayToMove;
}

// Assembles the room returns it to the caller
shared_ptr<Room> Game::buildRoom(const int& roomNum)
{
    // So let's build that now.
    auto room = make_shared<Room>();
    
    // First let's get the main description of the room
    shared_ptr<MainDescriptionSection> md = getDBSection<MainDescriptionSection>(SECTION_TYPE::MAIN_DESCRIPTIONS);
    room->setMainDescription(md->getDescription(roomNum));
    
    // And the short description too
    room->setShortDescription((getDBSection<ShortDescriptionSection>(SECTION_TYPE::SHORT_DESCRIPTIONS))->getDescription(roomNum));
    
    // Now for the room we are building, we have to add the possible destinations and the verbs
    // that can take the player there.
    shared_ptr<TravelSection> ts = getDBSection<TravelSection>(SECTION_TYPE::TRAVEL_TABLE);
    room->setTravelTable(ts->getVerbs2Rooms(roomNum));
    
	// Now let's populate the objects that are supposed to be in the room when it's
	// first built
	shared_ptr<ObjectLocationSection> ols = getDBSection<ObjectLocationSection>(SECTION_TYPE::OBJECT_LOCATIONS);

	// Get whatever objects are supposed to be initially placed in the room
	tuple<bool, vector<pair<int, int>>> objects = ols->getInitialObjectsForRoom(roomNum);

	// Are there any initial objects?
	if (get<0>(objects) == true)
	{
		// Yes, there are initial objects. Let's set them now
		for_each(get<1>(objects).begin(), get<1>(objects).end(), [&room](std::vector<pair<int, int>>::value_type& item)
		{
			// We convert the second item to a boolean (immovable) 'cause that's
			// more understandable
			room->setObject(item.first, item.second == 1 ? true : false);
		});
	}

	// And now we're done building the room.
    return room;
}

void Game::buildActionLogic()
{
    // This method is where we get into the details of game mechanics. For example,
    // how does the player "take" something? We have verbs that permit that, but
    // what does it mean, is it possible (e.g. is the object there, is it something
    // that *can* be taken, etc.).
    //
    // This method will put together the logic to handle all these scenarios, but
    // only enough so that we can recall them later and apply the logic to whatever
    // the current scenario is.
    //
    // The choices of actions is based on the original Fortran code, line 943, beginning
    // with the comment "ANALYSE A TRANSITIVE VERB". Since this is C++, not Fortran,
    // instead of using gotos as the original code did, we will use lambas, stored as
    // function objects, in the map, with the id of the action from the database (for
    // example, "TAKE" is 2001 in the database.
    // HOWEVER! We're not going to assume the number, we're going to assume the word,
    // based on the original Fortran. So first we're going to find the number in the
    // database and use that as the key when adding the logic.

    // Gonna need this for looking up IDs to populate the map
    shared_ptr<VerbsSection> vs = getDBSection<VerbsSection>(SECTION_TYPE::VOCABULARY);

    //
    // T A K E
    //
    int takeID = vs->getIDForVerb("TAKE");
    
    // And now we implement all the TAKE logic...
    function<void(const map<string,int>& logicParameters)> takeFunction = [&](const map<string,int>& logicParameters)
    {
        // From the original Fortran:
        // C  CARRY AN OBJECT.  SPECIAL CASES FOR BIRD AND CAGE (IF BIRD IN CAGE, CAN'T
        // C  TAKE ONE WITHOUT THE OTHER).  LIQUIDS ALSO SPECIAL, SINCE THEY DEPEND ON
        // C  STATUS OF BOTTLE.  ALSO VARIOUS SIDE EFFECTS, ETC.
       
        // We are looking for the kv "OBJECT_ID"
        auto it = logicParameters.find(LOGIC_OBJECT_KEY);
        assert(it != logicParameters.end());
        
        if (currentPlayer.hasObject(it->second))
            cout << "Already have it, but get this from the db in a good way";
        
        cout << "takeFunction! we're in room " << this-> currentRoom << endl;
		shared_ptr<ObjectSection> os = getDBSection<ObjectSection>(SECTION_TYPE::OBJECT_DESCRIPTION);
		// The database is set up so that objects co-mingle with other verbs in section 4. The
		// way to differentiate this is that objects begin at ID 1000, so to get the object
		// description, we need to subtract 1000 to get the proper value from section 5 of the
		// database
		shared_ptr<ObjectDescription> od = os->getDescriptionForObject(it->second - 1000);
		cout << "You want to take " << od->getMainDescription() << endl;
    };
    
    // And now store it in the map for future use
    actionLogic.insert(make_pair(takeID, takeFunction));

    //
    // Q U I T
    //
    int quitID = vs->getIDForVerb("QUIT");
    
    
    
    
}

void Game::printSection(const SECTION_TYPE& sectionNum)
{
    shared_ptr<Section> section = getDBSection(sectionNum);
    assert(section != nullptr);
    
    section->dumpDB();
}

void Game::buildWorld(const string& dbFile)
{
    cout << "Beginning to build the world from " << dbFile << endl;
    
    // Set up the sections that we'll use to parse the file
    addSectionObjects();
    
    // Our actual file object
    ifstream dbfile(dbFile);
    assert(dbfile.good());
    
    // Holds the line from the file
    string line;
    // What section of the database are we on?
    int dbSection = 0; // defaults to 1 at the very beginning
    // Are we currently reading from a particular section of the file?
    bool inSection = false;
    while (getline(dbfile, line))
    {
        if (inSection == false)
        {
            // We're "between" sections, in which case this line
            // contains the next section number
            dbSection = atoi(line.c_str());
            inSection = true;
            continue;
        }
        
        // Are we at the end of the file? The last value is 0, which we
        // interpret as the section, but because 0 was the default value
        // and because inSection was false initially, the only time we
        // could actually be in section 0 is when we've reached the end, in
        // which case we stop reading as there's nothing left to read
        if (dbSection == 0)
        {
            cout << "Reached end of database file" << endl;
            break;
        }
        
        // Now we're gonna tokenize the line on spaces
        vector<string> lineTokens = split(line, ' ');
        
        // The first element should always be a number, and if it's -1,
        // then we're at the end of a section
        int firstElem = atoi(lineTokens[0].c_str());
        
        // Are we at the end of a section?
        if (firstElem == -1)
        {
            // debugging
#ifdef DEBUG_GAME
            printSection(static_cast<SECTION_TYPE>(dbSection));
#endif // #ifdef DEBUG_GAME
            // yep, so set the boolean to indicate that and continue
            inSection = false;
            continue;
        }
        
        handleLine(static_cast<SECTION_TYPE>(dbSection), lineTokens);
    }
    
    // We're at the end of the database, so let's set up the world and its objects
    setUpWorld();
    
    cout << "Finished building the world." << endl;
}

void Game::gameLoop()
{
    const string HUH("Huh?");
    
    // This is the main game loop for receiving input from the player and returning the
    // result
    for(;;)
    {
        // First thing we do in the loop is print out whatever we're supposed to show
        // given the room, contents, has the player been here before, etc...
        showRoom();
        
        // Here we are actually getting whatever it is the player typed, which
        // may be all kinds of stuff, including garbage.
        string rawPlayerInput;
        cout << "--> ";
        getline(cin, rawPlayerInput);
        
        // Record what was typed in the player's move list
        this->currentPlayer.addMove(rawPlayerInput);
        
        // Okay, we only handle at most two words, separated by a space. So first
        // we need to convert whatever the player entered...
        vector<string> rawPlayerInputTokens = ::split(rawPlayerInput, ' ');
        // ... and throw away everything but the first two items by converting the vector
        // to a tuple. The two strings are the two items, the bool tells us whether there's
        // anything useful (i.e. were there any tokens in the vector?) and the int tells us
        // how many items are in the tuple (i.e. are both strings populated)
        tuple<bool, int, string, string> playerInput = getPlayerInput(rawPlayerInputTokens);
        
        //
        // Now we have the tuple of the player's input. Below we're going to determine what,
        // if any, action needs to take place
        //
        
        // Anything to do?
        if (get<0>(playerInput) == false)
        {
            // nope, player just hit enter, which we don't like
            cout << HUH << endl;
            continue;
        }
        
        // If we're here, there *is* something to do, which could be literally anything, the
        // player could have typed garbage. So before we get really into it, let's first see
        // whether this is something we can, in fact, handle
        shared_ptr<VerbsSection> verbs = getDBSection<VerbsSection>(SECTION_TYPE::VOCABULARY);
        if (verbs->canHandleVerb(get<2>(playerInput)) == false)
        {
            // They typed in something that we don't have, so just print a message and continue
            cout << HUH << endl;
            continue;
        }
        
        //
        // Okay, the player has entered something we can work with, so we're going to hand the strings
        // in the tuple over to the actual game logic method to have all the fun.
        //
        gameLogic(get<2>(playerInput), get<3>(playerInput));
        
        // Okay, after returning from gameLogic(), we've done whatever we need to as part of the game,
        // which may have resulted in the player's death or quitting.
        if (gameIsOver())
        {
            // Alright, so, we're done, so let's break out of the game loop now
            break;
        }
    }
}

// This function breaks the user input into a tuple which has the following fields:
//      0: bool     - is there anything in the tuple at all (i.e. is anything in the strings?)
//      1: int      - how many strings are populated (1 or 2)
//      2: string   - the first token
//      3: string   - the second token
// The player may have entered more than two tokens, but we don't care 'cause the game doesn't
// handle more than two
tuple<bool, int, string, string> Game::getPlayerInput(const vector<string>& rawPlayerInputTokens)
{
    tuple<bool, int, string, string> parsedInput;
    
    // Anything in the vector?
    if (rawPlayerInputTokens.size() == 0)
    {
        // Guess not
        get<0>(parsedInput) = false;
        get<1>(parsedInput) = 0;
        return parsedInput;
    }
    
    // If we're here, we have at least one item in the vector to send back to the loop
    
    // Get the first (and possibly only) token
    get<0>(parsedInput) = true;
    get<1>(parsedInput) = 1;
    
    // Ah, but before we add it to the tuple, we want to make it uppercase
    // because the database has everything in uppercase and we're not worried
    // about returning the player's input back to him or her
    string upperToken0(rawPlayerInputTokens[0]);
    std::transform(upperToken0.begin(), upperToken0.end(), upperToken0.begin(), ::toupper);
    get<2>(parsedInput) = upperToken0;
    
    // Is there another one? There may be more tokens, but we don't care because we only want two at
    // most (and here's where we get the second one)
    if (rawPlayerInputTokens.size() > 1)
    {
        // Yes there is, so we can set that too
        get<1>(parsedInput) = 2;
        
        // Same deal as before, make it uppercase
        string upperToken1(rawPlayerInputTokens[1]);
        std::transform(upperToken1.begin(), upperToken1.end(), upperToken1.begin(), ::toupper);
        get<3>(parsedInput) = upperToken1;
    }
    
    return parsedInput;
}

// Method in case the "game over" logic gets more complicated than a simple
// boolean
bool Game::gameIsOver()
{
    return gameOver;
}

// The user has moved somewhere or done something, so we
// need to show them the particular room they're in
void Game::showRoom()
{
	// Get the current room object
	shared_ptr<Room> room = roomMap.find(currentRoom)->second;

    // Here is where we're showing the description of the room the
    // player is in
	cout << room->getDescription() << endl;

	// Now we have to show whatever objects are currently in the room, which
	// is going to require an object description object:
	shared_ptr<ObjectSection> os = getDBSection<ObjectSection>(SECTION_TYPE::OBJECT_DESCRIPTION);

	// First we get the actual objects that are in the room
	vector<pair<int, bool>> objects = room->getObjects();

	// And for each item in the vector...
	for_each(objects.begin(), objects.end(), [&os](std::vector<pair<int, bool>>::value_type& object)
	{
		// ... get a description object ...
		shared_ptr<ObjectDescription> objDesc = os->getDescriptionForObject(object.first);
		// ... and show it's 000 description (for now) TODO: do the right thing here
		cout << objDesc->getSubDescription(0) << endl;
	});
}


void Game::gameLogic(const string& firstInput, const string& secondInput)
{
    //
    // Okay, let's see what we can do with the player's input
    //
    
    // First let's get our verb section..
    shared_ptr<VerbsSection> vs = getDBSection<VerbsSection>(SECTION_TYPE::VOCABULARY);
    // And get the ids for the inputs
    int firstInputID = vs->getIDForVerb(firstInput);
    int secondInputID = 0;
    if (secondInput.length())
    {
        secondInputID = vs->getIDForVerb(secondInput);
    }
    
    // Give the special section a chance first
    int specialIndex = firstInputID % 1000;     // firstInputID = N
    
    
    // This tells us whether this is a travel or object word. See the comment in
    // VerbsSection.h about this (and using they're variable names to keep things
    // consistent)
    int M = firstInputID / 1000; // firstInputID = N
    
    switch (M)
    {
        case 0:         // Motion
        {
            moveToRoom(firstInputID);   // firstInputID == travel verb
            break;
        }
        case 1:         // Object
            break;
        case 2:         // Action verb (e.g. carry, attack)
            performAction(firstInputID, secondInputID);
            break;
        case 3:         // Special case (e.g. dig)
            handleArbitraryMessages(specialIndex);
            break;
    };
}

void Game::performAction(const int& actionID, const int& objectID)
{
	// Look up the action in the actionLogic map
	auto action = actionLogic.find(actionID);
	
	// Did we find the action in the map?
	if (action != actionLogic.end())
	{
		// yep, so now execute the lamba logic
        map<string, int> actionKV;
        actionKV.insert(make_pair(LOGIC_OBJECT_KEY, objectID));
		action->second(actionKV);
	}
}

void Game::handleArbitraryMessages(const int& arbitraryMessageID)
{
    // The player entered input we deem as "arbitrary", so let's
    // show that.
    // First let's get our verb section..
    shared_ptr<ArbitraryMessagesSection> ams = getDBSection<ArbitraryMessagesSection>(SECTION_TYPE::ARBITRARY_MESSAGES);
    string message = ams->getMessageForID(arbitraryMessageID);
    printMessage(message);
}

//C  SECTION 3: TRAVEL TABLE.  EACH LINE CONTAINS A LOCATION NUMBER (X), A SECOND
//C	LOCATION NUMBER (Y), AND A LIST OF MOTION NUMBERS (SEE SECTION 4).
//C	EACH MOTION REPRESENTS A VERB WHICH WILL GO TO Y IF CURRENTLY AT X.
//C	Y, IN TURN, IS INTERPRETED AS FOLLOWS.  LET M=Y/1000, N=Y MOD 1000.
//C		IF N<=300	IT IS THE LOCATION TO GO TO.
//C		IF 300<N<=500	N-300 IS USED IN A COMPUTED GOTO TO
//C					A SECTION OF SPECIAL CODE.
//C		IF N>500	MESSAGE N-500 FROM SECTION 6 IS PRINTED,
//C					AND HE STAYS WHEREVER HE IS.
//C	MEANWHILE, M SPECIFIES THE CONDITIONS ON THE MOTION.
//C		IF M=0		IT'S UNCONDITIONAL.
//C		IF 0<M<100	IT IS DONE WITH M% PROBABILITY.
//C		IF M=100	UNCONDITIONAL, BUT FORBIDDEN TO DWARVES.
//C		IF 100<M<=200	HE MUST BE CARRYING OBJECT M-100.
//C		IF 200<M<=300	MUST BE CARRYING OR IN SAME ROOM AS M-200.
//C		IF 300<M<=400	PROP(M MOD 100) MUST *NOT* BE 0.
//C		IF 400<M<=500	PROP(M MOD 100) MUST *NOT* BE 1.
//C		IF 500<M<=600	PROP(M MOD 100) MUST *NOT* BE 2, ETC.
//C	IF THE CONDITION (IF ANY) IS NOT MET, THEN THE NEXT *DIFFERENT*
//C	"DESTINATION" VALUE IS USED (UNLESS IT FAILS TO MEET *ITS* CONDITIONS,
//C	IN WHICH CASE THE NEXT IS FOUND, ETC.).  TYPICALLY, THE NEXT DEST WILL
//C	BE FOR ONE OF THE SAME VERBS, SO THAT ITS ONLY USE IS AS THE ALTERNATE
//C	DESTINATION FOR THOSE VERBS.  FOR INSTANCE:
//C		15	110022	29	31	34	35	23	43
//C		15	14	29
//C	THIS SAYS THAT, FROM LOC 15, ANY OF THE VERBS 29, 31, ETC., WILL TAKE
//C	HIM TO 22 IF HE'S CARRYING OBJECT 10, AND OTHERWISE WILL GO TO 14.
//C		11	303008	49
//C		11	9	50
//C	THIS SAYS THAT, FROM 11, 49 TAKES HIM TO 8 UNLESS PROP(3)=0, IN WHICH
//C	CASE HE GOES TO 9.  VERB 50 TAKES HIM TO 9 REGARDLESS OF PROP(3).
void Game::moveToRoom(const int& travelVerb)
{
    // Okay, now let's see what we can do with the first ID from
    // the travel table
    auto it = roomMap.find(this->currentRoom);
    assert(it != roomMap.end());
    auto currentRoom = it->second;

    tuple<bool, vector<int>> destinations = currentRoom->getDestinationsForVerb(travelVerb);
    
    // Not using a lambda because when we find a room to move to, we want to stop
    // evaluating, and not continue through the vector
    for (std::vector<int>::const_iterator it = get<1>(destinations).begin(); it != get<1>(destinations).end(); ++it)
    {
        int Y = *it;
        int M = Y / 1000;
        int N = Y % 1000;
        
        if (N <= 300)
        {
            //
            // From the original code:
            //      "IT IS THE LOCATION TO GO TO."
            //
            
            // Set the room to the new location; if this returns true then we're
            // done and can break out of the loop, otherwise, we'll move on
            // to the next possibility
            if (setRoom(Y, M) == true)
                break;
        }
        else if ((300 < N) && (N <= 500))
        {
            //
            // From the original code:
            //      "N-300 IS USED IN A COMPUTED GOTO TO A SECTION OF SPECIAL CODE."
            //
            specialMove(N - 300);
            break;
        }
        else if (N > 500)
        {
            //
            // From the original code:
            //      "MESSAGE N-500 FROM SECTION 6 IS PRINTED, AND HE STAYS WHEREVER HE IS."
            //
            
            // Get our arbitrary messages section (section 6)
            shared_ptr<ArbitraryMessagesSection> ams = getDBSection<ArbitraryMessagesSection>(SECTION_TYPE::ARBITRARY_MESSAGES);
            // And print whatever we get for N - 500
            printMessage(ams->getMessageForID(N - 500));
            // And we're done, and we're not creating a new room
            break;
        }
    }
    
    // If we're here, we've either broken out of the loop and we are either now
    // in a new room, or all the tests failed and we're still in the old room. Either
    // way, we're done
}

void Game::specialMove(const int& computedMove)
{
    //C  SPECIAL MOTIONS COME HERE.  LABELLING CONVENTION: STATEMENT NUMBERS NNNXX
    //C  (XX=00-99) ARE USED FOR SPECIAL CASE NUMBER NNN (NNN=301-500).
    //
    //30000	NEWLOC=NEWLOC-300
    //GOTO (30100,30200,30300)NEWLOC
    //CALL BUG(20)
    //
    //C  TRAVEL 301.  PLOVER-ALCOVE PASSAGE.  CAN CARRY ONLY EMERALD.  NOTE: TRAVEL
    //C  TABLE MUST INCLUDE "USELESS" ENTRIES GOING THROUGH PASSAGE, WHICH CAN NEVER
    //C  BE USED FOR ACTUAL MOTION, BUT CAN BE SPOTTED BY "GO BACK".
    //
    //30100	NEWLOC=99+100-LOC
    //IF(HOLDNG.EQ.0.OR.(HOLDNG.EQ.1.AND.TOTING(EMRALD)))GOTO 2
    //NEWLOC=LOC
    //CALL RSPEAK(117)
    //GOTO 2
    //
    //C  TRAVEL 302.  PLOVER TRANSPORT.  DROP THE EMERALD (ONLY USE SPECIAL TRAVEL IF
    //C  TOTING IT), SO HE'S FORCED TO USE THE PLOVER-PASSAGE TO GET IT OUT.  HAVING
    //C  DROPPED IT, GO BACK AND PRETEND HE WASN'T CARRYING IT AFTER ALL.
    //
    //30200	CALL DROP(EMRALD,LOC)
    //GOTO 12
    //
    //C  TRAVEL 303.  TROLL BRIDGE.  MUST BE DONE ONLY AS SPECIAL MOTION SO THAT
    //C  DWARVES WON'T WANDER ACROSS AND ENCOUNTER THE BEAR.  (THEY WON'T FOLLOW THE
    //C  PLAYER THERE BECAUSE THAT REGION IS FORBIDDEN TO THE PIRATE.)  IF
    //C  PROP(TROLL)=1, HE'S CROSSED SINCE PAYING, SO STEP OUT AND BLOCK HIM.
    //C  (STANDARD TRAVEL ENTRIES CHECK FOR PROP(TROLL)=0.)  SPECIAL STUFF FOR BEAR.
    //
    //30300	IF(PROP(TROLL).NE.1)GOTO 30310
    //CALL PSPEAK(TROLL,1)
    //PROP(TROLL)=0
    //CALL MOVE(TROLL2,0)
    //CALL MOVE(TROLL2+100,0)
    //CALL MOVE(TROLL,PLAC(TROLL))
    //CALL MOVE(TROLL+100,FIXD(TROLL))
    //CALL JUGGLE(CHASM)
    //NEWLOC=LOC
    //GOTO 2
    //
    //30310	NEWLOC=PLAC(TROLL)+FIXD(TROLL)-LOC
    //IF(PROP(TROLL).EQ.0)PROP(TROLL)=1
    //IF(.NOT.TOTING(BEAR))GOTO 2
    //CALL RSPEAK(162)
    //PROP(CHASM)=1
    //PROP(TROLL)=2
    //CALL DROP(BEAR,NEWLOC)
    //FIXED(BEAR)=-1
    //PROP(BEAR)=3
    //IF(PROP(SPICES).LT.0)TALLY2=TALLY2+1
    //OLDLC2=NEWLOC
    //GOTO 99
    //
    //C  END OF SPECIALS.
}

// At the moment just prints out the message
void Game::printMessage(const string& message)
{
    cout << endl << message << endl;
}
