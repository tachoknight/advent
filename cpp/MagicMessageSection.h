//
//  MagicMessageSection.h
//  Advent
//
//  Created by Ron Olson on 9/22/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//

#ifndef __Advent__MagicMessageSection__
#define __Advent__MagicMessageSection__

#include "Section.h"

#include <map>
#include <string>
#include <vector>
#include <iostream>
#include <sstream>
using namespace std;

//C  SECTION 12: MAGIC MESSAGES. IDENTICAL TO SECTION 6 EXCEPT PUT IN A SEPARATE
//C	 SECTION FOR EASIER REFERENCE.  MAGIC MESSAGES ARE USED BY THE STARTUP,
//C	 MAINTENANCE MODE, AND RELATED ROUTINES.
class MagicMessageSection : public Section
{
    map<int, string> num2Desc;

public:
    MagicMessageSection() = default;

    virtual void addLine(int num, const vector<string>& lineTokens);
    virtual void dumpDB();
    virtual unsigned long getDBSize()
    {
        return num2Desc.size();
    }
};


#endif /* defined(__Advent__MagicMessageSection__) */
