#ifndef __Advent__ObjectLocationSection__
#define __Advent__ObjectLocationSection__

#include "Section.h"

#include <map>
#include <utility>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

//C SECTION 7: OBJECT LOCATIONS.  EACH LINE CONTAINS AN OBJECT NUMBER AND ITS
//C	INITIAL LOCATION (ZERO (OR OMITTED) IF NONE).  IF THE OBJECT IS
//C	IMMOVABLE, THE LOCATION IS FOLLOWED BY A "-1".  IF IT HAS TWO LOCATIONS
//C	(E.G. THE GRATE) THE FIRST LOCATION IS FOLLOWED WITH THE SECOND, AND
//C	THE OBJECT IS ASSUMED TO BE IMMOVABLE.

class ObjectLocationSection : public Section
{
    // This is the map of the object to a pair that indicates the room (first
    // int), and whether it's immovable (second int)
    map<int, pair<int, int>> obj2Room;

    // This is the map of the room the a list of pairs that indicates the
    // object (first int) and whether it's immovable (second int)
    map<int, vector<pair<int, int>>> room2Obj;
    
public:
    ObjectLocationSection() = default;

    virtual void addLine(int num, const vector<string>& lineTokens);
    virtual void dumpDB();
    virtual unsigned long getDBSize()
    {
        return obj2Room.size();
    }
    
    // Here begins the ObjectLocationSection-specific stuff

	// Get the list of objects for the given room number.
	// The return is a tuple of a bool and a vector. If the
	// room doesn't have any objects, then the bool will
	// be false. Otherwise, it will be true and the vector will
	// be populated with the objects to be placed in the room.
	// Note that this is only for initial objects; what the 
	// room has during gameplay is determined by the player
	tuple<bool, vector<pair<int, int>>> getInitialObjectsForRoom(const int& roomNum);
};

#endif /* defined(__Advent__ObjectLocationSection__) */