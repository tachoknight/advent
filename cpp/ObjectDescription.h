

#ifndef __Advent__ObjectDescription__
#define __Advent__ObjectDescription__

#include <string>
#include <vector>
#include <map>
#include <sstream>
#include <cassert>
#include <iostream>
#include <algorithm>
using namespace std;

// This class represents section 5, object descriptions. This section of the database is
// a little weird in that it has multiple messages for the multiple states the object can
// be in
class ObjectDescription
{
    // This is the first line of the database and represents the initial state
    string mainDescription = "";

    // This map contains the "sub number" that represents the alternate state of the object.
    // There can be more than one, so thus the map
    map<int, string> num2Description;

public:
    ObjectDescription() = default;

    void setMainDescription(const string& mainDescription)
    {
        this->mainDescription = mainDescription;
    }

    string getMainDescription() const
    {
        return this->mainDescription;
    }

    // This is similar to other methods in the *Section classes in that this
    // method will put the final line together
    void setSubDescriptionLine(const int& num, const string& subDescriptionPart)
    {
        // Search for the line
        auto it = num2Description.find(num);
        // find it?
        if (it == num2Description.end())
        {
            // no, so add it now
            num2Description.insert(std::make_pair(num, subDescriptionPart));
        }
        else
        {
            // yes, we did find it, so because we know that
            // the file works in sequence, the line we have
            // can be appended to the line already in the map
            stringstream newLine;
            newLine << it->second << " " << subDescriptionPart << '\n';
            it->second = newLine.str();
        }
    }

    string getSubDescription(const int& num) const
    {
        auto it = num2Description.find(num);
        assert(it != num2Description.end());

        return it->second;
    }

    void dumpDB()
    {
        for_each(num2Description.begin(), num2Description.end(), [](std::map<int, string>::value_type& kv)
        {
            cout << "\tObj SubID: " << kv.first << " - Description: " << kv.second << endl;
        });
    }
};


#endif /* defined(__Advent__ObjectDescription__) */