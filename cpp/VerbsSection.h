//
//  TravelSection.h
//  Advent
//
//  Created by Ron Olson on 9/16/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//

#ifndef __Advent__VerbSection__
#define __Advent__VerbSection__

#include "Section.h"

#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <set>
using namespace std;

//C  SECTION 4: VOCABULARY.  EACH LINE CONTAINS A NUMBER (N), A TAB, AND A
//C	FIVE-LETTER WORD.  CALL M=N/1000.  IF M=0, THEN THE WORD IS A MOTION
//C	VERB FOR USE IN TRAVELLING (SEE SECTION 3).  ELSE, IF M=1, THE WORD IS
//C	AN OBJECT.  ELSE, IF M=2, THE WORD IS AN ACTION VERB (SUCH AS "CARRY"
//C	OR "ATTACK").  ELSE, IF M=3, THE WORD IS A SPECIAL CASE VERB (SUCH AS
//C	"DIG") AND N MOD 1000 IS AN INDEX INTO SECTION 6.  OBJECTS FROM 50 TO
//C	(CURRENTLY, ANYWAY) 79 ARE CONSIDERED TREASURES (FOR PIRATE, CLOSEOUT).

class VerbsSection : public Section
{
    // The value is a vector because there can be multiple words that
    // do the same thing (e.g. key 11 which value is out, outsi, exit, and leave)
    map<int, vector<string>> num2Verb;

    // This is a set that holds all the unique verbs which we use to determine
    // whether the player entered valid input
    map<string, int> verb2Num;

public:
    VerbsSection() = default;

    virtual void addLine(int num, const vector<string>& lineTokens);
    virtual void dumpDB();
    virtual unsigned long getDBSize()
    {
        return num2Verb.size();
    }

    // And here are the methods that are specific to verbs
    bool canHandleVerb(const string& verb);
    int getIDForVerb(const string& verb);
};
#endif /* defined(__Advent__VerbSection__) */