//
//  MainDescriptionSection.cpp
//  Advent
//
//  Created by Ron Olson on 9/16/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//


#include "MainDescriptionSection.h"


void MainDescriptionSection::addLine(int num, const vector<string>& lineTokens)
{
    // Search for the line
    auto it = num2Desc.find(num);
    // find it?
    if (it == num2Desc.end())
    {
        // no, so add it now
        num2Desc.insert(std::make_pair(num, assembleLine(lineTokens)));
    }
    else
    {
        // yes, we did find it, so because we know that
        // the file works in sequence, the line we have
        // can be appended to the line already in the map
        string linePart(assembleLine(lineTokens));
        stringstream newLine;
        newLine << it->second << " " << linePart << '\n';
        it->second = newLine.str();
    }
}


void MainDescriptionSection::dumpDB()
{
    cout << "Dumping DB for MainDescriptionSection" << endl;
    for_each(num2Desc.begin(), num2Desc.end(),
             [](std::map<int, string>::value_type& kv)
    {
        cout << "Key: " << kv.first << "\nValue: " << kv.second << endl;
    });

    cout << "Finished dumping DB for MainDescriptionSection" << endl;
}

string MainDescriptionSection::getDescription(int id)
{
    auto it = num2Desc.find(id);
    assert (it != num2Desc.end());

    return it->second;
}

