//
//  ShortDescriptionSection.h
//  Advent
//
//  Created by Ron Olson on 9/16/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//

#ifndef __Advent__ShortDescriptionSection__
#define __Advent__ShortDescriptionSection__

#include "Section.h"

#include <iostream>
#include <map>
#include <vector>
#include <string>
using namespace std;

// Holds the short description of the room. This section is simpler than
// the main section insofar as the description is always on one line

//C  SECTION 2: SHORT FORM DESCRIPTIONS.  SAME FORMAT AS LONG FORM.  NOT ALL
//C	 PLACES HAVE SHORT DESCRIPTIONS.

class ShortDescriptionSection : public Section
{
    map<int, string> num2Desc;
public:
    ShortDescriptionSection() = default;

    virtual void addLine(int num, const vector<string>& lineTokens);
    virtual void dumpDB();
    virtual unsigned long getDBSize()
    {
        return num2Desc.size();
    }

    // Here begins the methods specific to this particular class
    string getDescription(int id);
};


#endif /* defined(__Advent__ShortDescriptionSection__) */
