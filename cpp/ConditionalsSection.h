//
//  ConditionalsSection.h
//  Advent
//
//  Created by Ron Olson on 9/22/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//

#ifndef __Advent__ConditionalsSection__
#define __Advent__ConditionalsSection__

#include "Section.h"

#include <iostream>
#include <map>
#include <vector>
#include <string>
using namespace std;

//C  SECTION 9: LIQUID ASSETS, ETC.  EACH LINE CONTAINS A NUMBER (N) AND UP TO 20
//C	LOCATION NUMBERS.  BIT N (WHERE 0 IS THE UNITS BIT) IS SET IN COND(LOC)
//C	FOR EACH LOC GIVEN.  THE COND BITS CURRENTLY ASSIGNED ARE:
//C		0	LIGHT
//C		1	IF BIT 2 IS ON: ON FOR OIL, OFF FOR WATER
//C		2	LIQUID ASSET, SEE BIT 1
//C		3	PIRATE DOESN'T GO HERE UNLESS FOLLOWING PLAYER
//C	OTHER BITS ARE USED TO INDICATE AREAS OF INTEREST TO "HINT" ROUTINES:
//C		4	TRYING TO GET INTO CAVE
//C		5	TRYING TO CATCH BIRD
//C		6	TRYING TO DEAL WITH SNAKE
//C		7	LOST IN MAZE
//C		8	PONDERING DARK ROOM
//C		9	AT WITT'S END
//C	COND(LOC) IS SET TO 2, OVERRIDING ALL OTHER BITS, IF LOC HAS FORCED
//C	MOTION.

class ConditionalsSection : public Section
{
    map<int, vector<int>> num2Locations;
public:
    ConditionalsSection() = default;

    virtual void addLine(int num, const vector<string>& lineTokens);
    virtual void dumpDB();
    virtual unsigned long getDBSize()
    {
        return num2Locations.size();
    }
};

#endif /* defined(__Advent__ConditionalsSection__) */
