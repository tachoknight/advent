//
//  Game.h
//  Advent
//
//  Created by Ron Olson on 9/16/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//

#ifndef __Advent__Game__
#define __Advent__Game__

#include "Section.h"
#include "Room.h"
#include "Player.h"

#include <time.h>
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <cstdlib>
#include <cassert>
#include <cstdint>
#include <tuple>
#include <memory>
#include <functional>
using namespace std;


//
// Our main game class which will be instantiated in main()
//
class Game
{
    // Our section types as an enum...
    enum class SECTION_TYPE : std::int8_t
    {
        MAIN_DESCRIPTIONS = 1,
        SHORT_DESCRIPTIONS = 2,
        TRAVEL_TABLE = 3,
        VOCABULARY = 4,
        OBJECT_DESCRIPTION = 5,
        ARBITRARY_MESSAGES = 6,
        OBJECT_LOCATIONS = 7,
        ACTION_DEFAULTS = 8,
        ASSETS = 9,
        CLASS_MESSAGES = 10,
        HINTS = 11,
        MAGIC_MESSAGES = 12
    };
    
    // ... and for unordered_map, which is a hashmap, we need to
    // provide a custom hashing function because we can't simply
    // cast to int like an old-time enum
    struct SectionEnumClassHash
    {
        template <typename T>
        std::size_t operator()(T t) const
        {
            return static_cast<std::size_t>(t);
        }
    };
    
    // For holding the various parts of the database as we're reading through
    unordered_map<SECTION_TYPE, shared_ptr<Section>, SectionEnumClassHash> dbSections;

    // Our map of rooms. The integer refers to the room number, which
    // is based on the number from the database in both the first and
    // second sections.
    // We are using a pointer (shared_ptr) so that we can update the status
    // of the room in place without expensive copying
    map<int, shared_ptr<Room>> roomMap;

    // The various keys for the map below
    const string LOGIC_OBJECT_KEY = "OBJECT_ID";

    // This is a map of game logic. It's configured to be verb id to function that
    // will actually do the computing and setting for each of those verbs
    // The function takes a kv map of string to int. The reason for this extra step
    // is to future-proof the possibility that we may need to actively pass more than
    // a single parameter, of a specific type (say, int) and then have to resort to
    // setting special members variables just to hold temporary values.
    unordered_map<int, function<void(const map<string, int>& logicParameters)>> actionLogic;
    
    //
    // Here begins the variables we need to hold onto for the player
    //

    // The room the player is currently in (defaults to 1 at the beginning of the
    // game)
    int currentRoom = 0;

    // This boolean is set in the game logic method to determine that the game is over
    // either by the player dying or quitting (or, I guess, even winning)
    bool gameOver = false;
    
    // The player; holds all the state related to the player (e.g. inventory, score, etc.)
    Player currentPlayer;
    
public:
    Game(const string& dbFile) : currentRoom(1), gameOver(false)
    {
        cout << "Starting game..." << endl;

        // First thing first is to actually set up the game by
        // reading in the database file, setting up the various objects,
        // etc.
        initializeGame(dbFile);

        // And here the game actually starts
        gameLoop();

        // Since we're returned from gameLoop(), the game is over
        // and we need to shut down
        cleanupGame();
    }

    ~Game()
    {
        cout << "Game is shutting down..." << endl;
    }

private:
    Game() = default;

    void initializeGame(const string& dbFile);
    void cleanupGame();
    void addSectionObjects();

    // Used when loading the database
    shared_ptr<Section> getDBSection(const SECTION_TYPE& sectionNum);
    // Used to get a specific object from the map
    template <class T> shared_ptr<T> getDBSection(const SECTION_TYPE& sectionNum);

    void handleLine(const SECTION_TYPE& sectionNum, const vector<string>& lineTokens);
    void setUpWorld();
    void printSection(const SECTION_TYPE& sectionNum);
    void buildWorld(const string& dbFile);
    // This is called after the database is set up and is where we get into specifics that
    // tie into the actual game logic. For example, "take" refers to picking up an object
    // and storing it in the player inventory. However, there are multiple variations of
    // take (by the ID in the database) and we want to handle all the situations where
    // the ID is what ultimately resolves to "take", and all its accompanying mechanisms.
    // The logic for all those actions are created here and stored in the action map
    // so we can look it up later
    void buildActionLogic();
    // Calls buildRoom below. Separate method becuause we may do additional work after the room
    // has been built. Note that because of the motionCondition field (check the comments in the
    // method code), this may return *false*, because the player did not meet the condition. If
    // that's the case, then the room will *not* be set to roomNum.
    bool setRoom(const int& roomNum, const int& motionCondition);
    // Returns a fully-built room for the given room number, with all the defaults in place.
    // This is a shared_ptr 'cause that's what we're going to store in the map above, so that
    // any changes to the room state can be maintained without expensive copying.
    shared_ptr<Room> buildRoom(const int& roomNum);
    // Converts the raw player input into a tuple we can work with
    tuple<bool, int, string, string> getPlayerInput(const vector<string>& rawPlayerInputTokens);
    // The method that actually receives the user input and does some sanity checking
    void gameLoop();
    // The game logic method where we figure everything out (calling out to helpers when
    // necessary
    void gameLogic(const string& firstInput, const string& secondInput);
    // Check whether the player quit or died as a consequence of the game logic
    bool gameIsOver();
    // This method prints the room description and whatever else the player needs to know
    void showRoom();
    // Does all the work to move us to another room (possibly calculated, which is why we
    // have multiple possible destinations, which is why we have a vector of ints)
    void moveToRoom(const int& travelVerb);
    // Prints various things outside of the room descriptions
    void printMessage(const string& message);
    // For handling special cases of inputs the player entered (e.g. "dig")
    void handleArbitraryMessages(const int& arbitraryMessageID);
    // Our method for handling "special" moves (e.g. plover transport, troll bridge...)
    void specialMove(const int& computedMove);
    // Deals with all the "action" things
    void performAction(const int& actionID, const int& objectID);
};


#endif /* defined(__Advent__Game__) */
