//
//  ActionDefaultsSection.h
//  Advent
//
//  Created by Ron Olson on 9/22/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//

#ifndef __Advent__ActionDefaultsSection__
#define __Advent__ActionDefaultsSection__

#include "Section.h"

#include <iostream>
#include <map>
#include <vector>
#include <string>
using namespace std;

// Section 8 of the file: Action Defaults. From the Fortran code:
// C  SECTION 8: ACTION DEFAULTS.  EACH LINE CONTAINS AN "ACTION-VERB" NUMBER AND
// C	THE INDEX (IN SECTION 6) OF THE DEFAULT MESSAGE FOR THE VERB.

class ActionDefaultsSection : public Section
{
    map<int, int> num2Index;
public:
    ActionDefaultsSection() = default;

    virtual void addLine(int num, const vector<string>& lineTokens);
    virtual void dumpDB();
    virtual unsigned long getDBSize()
    {
        return num2Index.size();
    }
};

#endif /* defined(__Advent__ActionDefaultsSection__) */
