//
//  ShortDescriptionSection.cpp
//  Advent
//
//  Created by Ron Olson on 9/16/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//

#include "ShortDescriptionSection.h"

void ShortDescriptionSection::addLine(int num, const vector<string>& lineTokens)
{
    num2Desc.insert(std::make_pair(num, assembleLine(lineTokens)));
}

void ShortDescriptionSection::dumpDB()
{
    cout << "Dumping DB for ShortDescriptionSection" << endl;
    for_each(num2Desc.begin(), num2Desc.end(),
             [](std::map<int, string>::value_type& kv)
    {
        cout << "Key: " << kv.first << "\nValue: " << kv.second << endl;
    });

    cout << "Finished dumping DB for ShortDescriptionSection" << endl;
}

string ShortDescriptionSection::getDescription(int id)
{
    auto it = num2Desc.find(id);
    assert (it != num2Desc.end());

    return it->second;
}
