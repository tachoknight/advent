//
//  TravelSection.cpp
//  Advent
//
//  Created by Ron Olson on 9/16/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//

#include "TravelSection.h"

void TravelSection::addLine(int num, const vector<string>& lineTokens)
{
    // We don't want the first element in the vector, which is the num
    vector<string> fixedLineTokens;
    std::copy(lineTokens.begin() + 1, lineTokens.end(), back_inserter(fixedLineTokens));

    // Search for the line
    auto it = num2TravelLists.find(num);
    // find it?
    if (it == num2TravelLists.end())
    {
        // no, so add it now, which we need to do by creating a new
        // list...
        vector<vector<string>> listOfLists;
        // ... and add the list of tokens to it...
        listOfLists.push_back(fixedLineTokens);
        // ... and finally add it to the map
        num2TravelLists.insert(std::make_pair(num, listOfLists));
    }
    else
    {
        // add our new list to the list o' lists
        it->second.push_back(fixedLineTokens);
    }
}

void TravelSection::dumpDB()
{
    std::cout << "Dumping DB for TravelSection" << endl;

    // So, yeah, wow. Well, this is a map of int to a list of lists, so...
    for_each(num2TravelLists.begin(), num2TravelLists.end(),
             [](std::map<int, vector<vector<string>>>::value_type& kv)
    {
        // Here's the int key
        std::cout << "Key: " << kv.first << endl;
        // Here's the list of lists
        for_each(kv.second.begin(), kv.second.end(),
                 [](std::vector<vector<string>>::value_type& listOfLists)
        {
            std::cout << "\t";
            // Here's an individual list
            for_each(listOfLists.begin(), listOfLists.end(),
                     [](std::vector<string>::value_type& travelList)
            {
                std::cout << travelList << " ";

            });

            std::cout << endl;
        });
    });


    std::cout << "Finished dumping DB for TravelSection" << endl;
}

map<int, vector<int>> TravelSection::getVerbs2Rooms(int fromRoom)
{
    // What we're ultimately going to return
    map<int, vector<int>> verbs2Rooms;

    // Search for the room
    auto it = num2TravelLists.find(fromRoom);
    // find it?
    assert(it != num2TravelLists.end());

    for_each(it->second.begin(), it->second.end(),
             [&verbs2Rooms](std::vector<vector<string>>::value_type& listOfLists)
    {
        // The structure of our listofLists is that contains a vector
        // where the first element is going to be the room to go to,
        // while the rest are verbs.

        // The first item in the vector is the room to go to
        int destinationRoom = stoi(listOfLists[0]);

        for_each(listOfLists.begin() + 1, listOfLists.end(),
                 [&destinationRoom, &verbs2Rooms](std::vector<string>::value_type& verb)
        {
            auto verbIt = verbs2Rooms.find(stoi(verb));
            if (verbIt == verbs2Rooms.end())
            {
                // Never seen this verb before, so let's add it to
                // the list
                vector<int> toRoomVector;
                toRoomVector.push_back(destinationRoom);
                verbs2Rooms.insert(std::make_pair(stoi(verb), toRoomVector));
            }
            else
            {
                // Ah! We *have* seen this verb before, which means that
                // this verb has multiple destinations based on the "to" room
                // number (which may be a calculated value). So by adding the
                // room number to the vector, that means that if the calculation
                // room that is already in the vector fails, we'll be falling into
                // this room (unless this room is *also* a calculation, in which
                // case the situation keeps repeating).
                verbIt->second.push_back(destinationRoom);
            }

        });

    });


    return verbs2Rooms;
}