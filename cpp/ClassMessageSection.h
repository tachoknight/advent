//
//  ClassMessageSection.h
//  Advent
//
//  Created by Ron Olson on 9/22/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//

#ifndef __Advent__ClassMessageSection__
#define __Advent__ClassMessageSection__

#include "Section.h"

#include <iostream>
#include <map>
#include <vector>
#include <string>
using namespace std;

//C  SECTION 10: CLASS MESSAGES.  EACH LINE CONTAINS A NUMBER (N), A TAB, AND A
//C	MESSAGE DESCRIBING A CLASSIFICATION OF PLAYER.  THE SCORING SECTION
//C	SELECTS THE APPROPRIATE MESSAGE, WHERE EACH MESSAGE IS CONSIDERED TO
//C	APPLY TO PLAYERS WHOSE SCORES ARE HIGHER THAN THE PREVIOUS N BUT NOT
//C	HIGHER THAN THIS N.  NOTE THAT THESE SCORES PROBABLY CHANGE WITH EVERY
//C	MODIFICATION (AND PARTICULARLY EXPANSION) OF THE PROGRAM.
class ClassMessageSection : public Section
{
    map<int, string> num2Desc;
public:
    ClassMessageSection() = default;

    virtual void addLine(int num, const vector<string>& lineTokens);
    virtual void dumpDB();
    virtual unsigned long getDBSize()
    {
        return num2Desc.size();
    }
};


#endif /* defined(__Advent__ClassMessageSection__) */
