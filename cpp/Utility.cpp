//
//  Utility.cpp
//  Advent
//
//  Created by Ron Olson on 9/16/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//

#include "Utility.h"

// Even C++11 doesn't have trim? WTF?
string trim(const string& str)
{
    string fixedString(str);
    fixedString.erase(0, str.find_first_not_of(' '));
    fixedString.erase(str.find_last_not_of(' ') + 1);
    return fixedString;
}

// Take string, split on delim, return vector of tokens
vector<string> split(const string& s, char delim)
{
    string token;
    vector<string> tokens;

    for_each(s.begin(), s.end(), [&](char c)
    {
        if (!isspace(c))
            token += c;
        else
        {
            if (token.length()) tokens.push_back(token);
            token.clear();
        }
    });

    if (token.length())
        tokens.push_back(token);

    return tokens;
}