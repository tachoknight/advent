//
//  HintsSection.h
//  Advent
//
//  Created by Ron Olson on 9/22/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//

#ifndef __Advent__HintsSection__
#define __Advent__HintsSection__

#include "Section.h"

#include <iostream>
#include <map>
#include <vector>
#include <string>
using namespace std;

//C  SECTION 11: HINTS.  EACH LINE CONTAINS A HINT NUMBER (CORRESPONDING TO A
//C	COND BIT, SEE SECTION 9), THE NUMBER OF TURNS HE MUST BE AT THE RIGHT
//C	LOC(S) BEFORE TRIGGERING THE HINT, THE POINTS DEDUCTED FOR TAKING THE
//C	HINT, THE MESSAGE NUMBER (SECTION 6) OF THE QUESTION, AND THE MESSAGE
//C	NUMBER OF THE HINT.  THESE VALUES ARE STASHED IN THE "HINTS" ARRAY.
//C	HNTMAX IS SET TO THE MAX HINT NUMBER (<= HNTSIZ).  NUMBERS 1-3 ARE
//C	UNUSABLE SINCE COND BITS ARE OTHERWISE ASSIGNED, SO 2 IS USED TO
//C	REMEMBER IF HE'S READ THE CLUE IN THE REPOSITORY, AND 3 IS USED TO
//C	REMEMBER WHETHER HE ASKED FOR INSTRUCTIONS (GETS MORE TURNS, BUT LOSES
//C	POINTS).

class HintsSection : public Section
{
    map<int, vector<int>> num2Hint;
public:
    HintsSection() = default;

    virtual void addLine(int num, const vector<string>& lineTokens);
    virtual void dumpDB();
    virtual unsigned long getDBSize()
    {
        return num2Hint.size();
    }
};


#endif /* defined(__Advent__HintsSection__) */
