//
//  TravelSection.h
//  Advent
//
//  Created by Ron Olson on 9/16/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//

#ifndef __Advent__TravelSection__
#define __Advent__TravelSection__

#include "Section.h"

#include <iostream>
#include <map>
#include <vector>
#include <string>
using namespace std;

//C SECTION 3: TRAVEL TABLE.  EACH LINE CONTAINS A LOCATION NUMBER (X), A SECOND
//C	LOCATION NUMBER (Y), AND A LIST OF MOTION NUMBERS (SEE SECTION 4).
//C	EACH MOTION REPRESENTS A VERB WHICH WILL GO TO Y IF CURRENTLY AT X.
//C	Y, IN TURN, IS INTERPRETED AS FOLLOWS.  LET M=Y/1000, N=Y MOD 1000.
//C		IF N<=300	IT IS THE LOCATION TO GO TO.
//C		IF 300<N<=500	N-300 IS USED IN A COMPUTED GOTO TO
//C					A SECTION OF SPECIAL CODE.
//C		IF N>500	MESSAGE N-500 FROM SECTION 6 IS PRINTED,
//C					AND HE STAYS WHEREVER HE IS.
//C	MEANWHILE, M SPECIFIES THE CONDITIONS ON THE MOTION.
//C		IF M=0		IT'S UNCONDITIONAL.
//C		IF 0<M<100	IT IS DONE WITH M% PROBABILITY.
//C		IF M=100	UNCONDITIONAL, BUT FORBIDDEN TO DWARVES.
//C		IF 100<M<=200	HE MUST BE CARRYING OBJECT M-100.
//C		IF 200<M<=300	MUST BE CARRYING OR IN SAME ROOM AS M-200.
//C		IF 300<M<=400	PROP(M MOD 100) MUST *NOT* BE 0.
//C		IF 400<M<=500	PROP(M MOD 100) MUST *NOT* BE 1.
//C		IF 500<M<=600	PROP(M MOD 100) MUST *NOT* BE 2, ETC.
//C	IF THE CONDITION (IF ANY) IS NOT MET, THEN THE NEXT *DIFFERENT*
//C	"DESTINATION" VALUE IS USED (UNLESS IT FAILS TO MEET *ITS* CONDITIONS,
//C	IN WHICH CASE THE NEXT IS FOUND, ETC.).  TYPICALLY, THE NEXT DEST WILL
//C	BE FOR ONE OF THE SAME VERBS, SO THAT ITS ONLY USE IS AS THE ALTERNATE
//C	DESTINATION FOR THOSE VERBS.  FOR INSTANCE:
//C		15	110022	29	31	34	35	23	43
//C		15	14	29
//C	THIS SAYS THAT, FROM LOC 15, ANY OF THE VERBS 29, 31, ETC., WILL TAKE
//C	HIM TO 22 IF HE'S CARRYING OBJECT 10, AND OTHERWISE WILL GO TO 14.
//C		11	303008	49
//C		11	9	50
//C	THIS SAYS THAT, FROM 11, 49 TAKES HIM TO 8 UNLESS PROP(3)=0, IN WHICH
//C	CASE HE GOES TO 9.  VERB 50 TAKES HIM TO 9 REGARDLESS OF PROP(3).

class TravelSection : public Section
{
    // This is one of the most complicated part of the database, in
    // that this section contains, for each room (the int as key) a list
    // of lists, which will be parsed when setting up the room object, but
    // for right now we want to keep it in an unparsed state as parsing
    // will be done later
    map<int, vector<vector<string>>> num2TravelLists;
public:
    TravelSection() = default;

    virtual void addLine(int num, const vector<string>& lineTokens);
    virtual void dumpDB();
    virtual unsigned long getDBSize()
    {
        return num2TravelLists.size();
    }

    // And here we begin our special Travel stuff

    // Transform our travel list into the format the room class wants
    map<int, vector<int>> getVerbs2Rooms(int fromRoom);
};

#endif /* defined(__Advent__TravelSection__) */
