//
//  ActionDefaultsSection.cpp
//  Advent
//
//  Created by Ron Olson on 9/22/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//

#include "ActionDefaultsSection.h"

void ActionDefaultsSection::addLine(int num, const vector<string>& lineTokens)
{
    num2Index.insert(std::make_pair(num, atoi(lineTokens[1].c_str())));
}

void ActionDefaultsSection::dumpDB()
{
    cout << "Dumping DB for ActionDefaultsSection" << endl;
    for_each(num2Index.begin(), num2Index.end(),
             [](std::map<int, int>::value_type& kv)
    {
        cout << "Key: " << kv.first << "\nValue: " << kv.second << endl;
    });

    cout << "Finished dumping DB for ActionDefaultsSection" << endl;

}