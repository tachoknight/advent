
#include "VerbsSection.h"

void VerbsSection::addLine(int num, const vector<string>& lineTokens)
{
    // Search for the line
    auto it = num2Verb.find(num);
    // find it?
    if (it == num2Verb.end())
    {
        // no, so add it now, which we need to do by creating a new
        // list...
        vector<string> listOfVerbs;
        // ... and add the list of tokens to it...
        listOfVerbs.push_back(trim(assembleLine(lineTokens)));
        // ... and finally add it to the map
        num2Verb.insert(std::make_pair(num, listOfVerbs));
    }
    else
    {
        // add our new list to the list o' lists
        it->second.push_back(trim(assembleLine(lineTokens)));
    }

    // And add it to our big bag o' verbs
    verb2Num.insert(std::make_pair(trim(assembleLine(lineTokens)), num));
}

void VerbsSection::dumpDB()
{
    cout << "Dumping DB for VerbsSection" << endl;
    cout << "Num2Verbs:------------" << endl;
    for_each(num2Verb.begin(), num2Verb.end(),
             [](std::map<int, vector<string>>::value_type& kv)
    {
        cout << "Key: " << kv.first << endl;
        for_each(kv.second.begin(), kv.second.end(), [](std::vector<string>::value_type& val)
        {
            cout << "\t" << val << endl;
        });
    });

    cout << "Unique verbs:------------" << endl;
    for(const auto& verb : verb2Num)
    {
        cout << verb.first << " - " << verb.second << endl;
    }

    cout << "Finished dumping DB for VerbsSection" << endl;
}

bool VerbsSection::canHandleVerb(const string& verb)
{
    // Does the verb exist in our list (set)? Used to
    // determine whether the player entered something that
    // the game can act on
    if (verb2Num.find(verb) == verb2Num.end())
        return false;

    return true;
}

int VerbsSection::getIDForVerb(const string& verb)
{
    auto it = verb2Num.find(verb);
    assert(it != verb2Num.end());
    
    return it->second;
}

