#ifndef __Advent__ObjectSection__
#define __Advent__ObjectSection__

#include "Section.h"
#include "ObjectDescription.h"

#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <memory>
using namespace std;

//C  SECTION 5: OBJECT DESCRIPTIONS.  EACH LINE CONTAINS A NUMBER (N), A TAB,
//C	AND A MESSAGE.  IF N IS FROM 1 TO 100, THE MESSAGE IS THE "INVENTORY"
//C	MESSAGE FOR OBJECT N.  OTHERWISE, N SHOULD BE 000, 100, 200, ETC., AND
//C	THE MESSAGE SHOULD BE THE DESCRIPTION OF THE PRECEDING OBJECT WHEN ITS
//C	PROP VALUE IS N/100.  THE N/100 IS USED ONLY TO DISTINGUISH MULTIPLE
//C	MESSAGES FROM MULTI-LINE MESSAGES; THE PROP INFO ACTUALLY REQUIRES ALL
//C	MESSAGES FOR AN OBJECT TO BE PRESENT AND CONSECUTIVE.  PROPERTIES WHICH
//C	PRODUCE NO MESSAGE SHOULD BE GIVEN THE MESSAGE ">$<".

class ObjectSection : public Section
{
    // Map that holds our id-to-description objects
    map<int, shared_ptr<ObjectDescription>> num2Description;

    // For loading the map, an object can have multiple
    // descriptions, but the database file does everything
    // the same way, so we have to maintain a state variable
    // to tell which id we're working with
    int	currentID = 0;

public:
    ObjectSection() = default;

    virtual void addLine(int num, const vector<string>& lineTokens);
    virtual void dumpDB();
    virtual unsigned long getDBSize()
    {
        return num2Description.size();
    }

	// And here begins the methods specific to this class
	shared_ptr<ObjectDescription> getDescriptionForObject(const int& objNum) const;
};

#endif /* defined(__Advent__ObjectSection__) */