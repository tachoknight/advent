//
//  Player.h
//  Advent
//
//  Created by Ron Olson on 10/22/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//

#ifndef __Advent__Player__
#define __Advent__Player__

#include <set>
#include <vector>
#include <string>
using namespace std;

// This class represents the, wait for it, player in the game.
// It holds the player inventory, move list, etc.
class Player
{
    // Holds everything the player entered, regardless of whether
    // it was handled by the game engine (i.e. everything the person
    // typed). - For debugging
    vector<string> moveList;
    
    // The player's inventory. This is a set to maximize
    // lookups
    set<int> inventoryList;
public:
    Player() = default;

    void addMove(const string& playerInput)
    {
        moveList.push_back(playerInput);
    }
    
    // Simple check to determine whether the player has "objectID"
    // in his or her inventory
    bool hasObject(int objectID);

	// Adds an object to the inventory
	void addObjectToInventory(int objectID);
};

#endif /* defined(__Advent__Player__) */
