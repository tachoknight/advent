//
//  ClassMessageSection.cpp
//  Advent
//
//  Created by Ron Olson on 9/22/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//

#include "ClassMessageSection.h"

void ClassMessageSection::addLine(int num, const vector<string>& lineTokens)
{
    num2Desc.insert(std::make_pair(num, assembleLine(lineTokens)));
}

void ClassMessageSection::dumpDB()
{
    cout << "Dumping DB for ClassMessageSection" << endl;
    for_each(num2Desc.begin(), num2Desc.end(),
             [](std::map<int, string>::value_type& kv)
    {
        cout << "Key: " << kv.first << "\nValue: " << kv.second << endl;
    });

    cout << "Finished dumping DB for ClassMessageSection" << endl;

}