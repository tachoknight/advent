//
//  Room.h
//  Advent
//
//  Created by Ron Olson on 9/16/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//

#ifndef __Advent__Room__
#define __Advent__Room__

#include <string>
#include <vector>
#include <map>
using namespace std;

class Room
{
    // The main description, which is what the player sees the
    // first time they come in
    string mainDescription = "";

    // Subsequent visits to the room will show the short description,
    // unless they explicitly look
    string shortDescription = "";

    // flag to determine whether the user has already seen the room
    mutable bool alreadyVisited = false;

    // This is our travel map. The key is the particular verb that can
    // be handled and the room it will take the player to. The reason why
    // the value is a vector is because a verb can take the player to multiple
    // rooms based on the calculation based on the destnation (the number in
    // the vector). It's also a vector because the rooms are prioritized based
    // on the sequence.
    //      o Key: The verb from section 3, column 3 (1-based) onwards.
    //      o Value: vector of destination rooms, section 3, column 2 (1-based).
    //               If the destination room is calculated and doesn't allow access,
    //               then the player will go to the next room in the vector, and we
    //               try again.
    map<int, vector<int>> verb2Rooms;

	// This is the list of objects in the room, whether by default or by
	// the player dropping something in it
    vector<pair<int, bool>> objects;

public:
    Room() = default;

    void setVerb2Destination(const int& verb, const int& destination)
    {
        // Search for the verb
        auto it = verb2Rooms.find(verb);
        // find it?
        if (it == verb2Rooms.end())
        {
            // no, so add it now, which we need to do by creating a new
            // list...
            vector<int> roomList;
            // ... and add the destination room to it..
            roomList.push_back(destination);
            // ... and finally both to the map
            verb2Rooms.insert(std::make_pair(verb, roomList));
        }
        else
        {
            // add the destination to the existing list
            it->second.push_back(destination);
        }
    }

    Room& setMainDescription(const string& mainDescription)
    {
        this->mainDescription = mainDescription;
        return *this;
    }

    Room& setShortDescription(const string& shortDescription)
    {
        this->shortDescription = shortDescription;
        return *this;
    }

    // Helper function so we don't have to make an explicit distinction
    // in the Game object
    string getDescription(bool showMainDescription = false) const
    {
        if (showMainDescription == false && alreadyVisited)
            return getShortDescription();

        return getMainDescription();
    }

    string getMainDescription() const
    {
        // since we're getting the main description (whether for
        // the first time or an explicit look), we want to set the
        // alreadyVisited flag so that any subsequent moves to this
        // room show the short description
        alreadyVisited = true;

        return this->mainDescription;
    }

    string getShortDescription() const
    {
        return this->shortDescription;
    }

    // Here we set the travel table which we'll use to navigate to
    // the other rooms
    void setTravelTable(const map<int, vector<int>>& verbs2Rooms)
    {
        this->verb2Rooms = verbs2Rooms;
    }
    
    // Get the destinations for the particular verb ID. It's a tuple
    // with two values; a bool to indicate that we did, in fact, find
    // a destination for the verb, in which case the second item in
    // the tuple will be the vector. If the bool is false, that means
    // we didn't find the verb, but that may mean the user typed in
    // a non-travel verb (e.g. "help") which means it should be handled
    // by another part of the code
    tuple<bool, vector<int>> getDestinationsForVerb(const int& verb)
    {
        tuple<bool, vector<int>> destinations;
        
        auto it = verb2Rooms.find(verb);
        if (it == verb2Rooms.end())
        {
            get<0>(destinations) = false;
        }
        else
        {
            get<0>(destinations) = true;
            get<1>(destinations) = it->second;
        }
        
        return destinations;
    }

	// This method adds an object to the list of objects that are currently
	// in the room
	void setObject(const int& objectNum, const bool isImmovable)
	{
		pair<int, bool> newObject;
		get<0>(newObject) = objectNum;
		get<1>(newObject) = isImmovable;
		objects.push_back(newObject);
	}

	vector<pair<int, bool>> getObjects() const
	{
		return this->objects;
	}

};


#endif /* defined(__Advent__Room__) */
