//
//  HintsSection.cpp
//  Advent
//
//  Created by Ron Olson on 9/22/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//

#include "HintsSection.h"

void HintsSection::addLine(int num, const vector<string>& lineTokens)
{
    // Ultimately we want this as a vector of ints, not strings, and bypass
    // the first element, soooo...
    vector<int> items;

    // beginning at the second element, copy the lineTokens vector into the items vector, converting along the way...
    transform(lineTokens.begin() + 1, lineTokens.end(), back_inserter(items), [] (const string& token)
    {
        return stoi(token);
    });

    // This section does not use multiple lines, so we don't have to worry about appending vectors
    num2Hint.insert(std::make_pair(num, items));
}

void HintsSection::dumpDB()
{
    cout << "Dumping DB for HintsSection" << endl;

    for_each(num2Hint.begin(), num2Hint.end(),
             [](std::map<int, vector<int>>::value_type& kv)
    {
        cout << "Key: " << kv.first;

        for_each(kv.second.begin(), kv.second.end(), [](std::vector<int>::value_type& num)
        {
            cout << " " << num ;
        });

        cout << endl;
    });


    cout << "Finished dumping DB for HintsSection" << endl;
}