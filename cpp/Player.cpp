//
//  Player.cpp
//  Advent
//
//  Created by Ron Olson on 10/22/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//

#include "Player.h"

bool Player::hasObject(int objectID)
{
    // Does the player have this object?
    auto it = inventoryList.find(objectID);
    
    if (it == inventoryList.end())
        return false;
    
    return true;
}

void Player::addObjectToInventory(int objectID)
{
	inventoryList.insert(objectID);
}