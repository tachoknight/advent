#ifndef __Advent__ArbitraryMessagesSection__
#define __Advent__ArbitraryMessagesSection__

#include "Section.h"

#include <map>
#include <string>
#include <vector>
#include <iostream>
#include <sstream>
using namespace std;

//C SECTION 6: ARBITRARY MESSAGES.  SAME FORMAT AS SECTIONS 1, 2, AND 5, EXCEPT
//C	THE NUMBERS BEAR NO RELATION TO ANYTHING (EXCEPT FOR SPECIAL VERBS
//C	IN SECTION 4).

class ArbitraryMessagesSection : public Section
{
    map<int, string> num2Desc;

public:
    ArbitraryMessagesSection() = default;

    virtual void addLine(int num, const vector<string>& lineTokens);
    virtual void dumpDB();
    virtual unsigned long getDBSize()
    {
        return num2Desc.size();
    }
    
    // Special stuff for Arbitrary Messages begin here
    string getMessageForID(const int& id) const;
};

#endif /* defined(__Advent__ArbitraryMessagesSection__) */
