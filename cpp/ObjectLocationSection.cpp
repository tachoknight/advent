#include "ObjectLocationSection.h"

void ObjectLocationSection::addLine(int num, const vector<string>& lineTokens)
{
    // The second item may not be populated, in which case it will be 0
    pair<int, int> statePair;
    statePair.first = atoi(lineTokens[1].c_str());
    if (lineTokens.size() == 3)
        statePair.second = atoi(lineTokens[2].c_str());

    obj2Room.insert(std::make_pair(num, statePair));
    
    // And now we want to do the same thing but in reverse, handle the
    // room to object id. This is used for intially populating the room
    // with the correct objects and to prevent looping through the
    // obj2Room map

    // Gonna need to rearrange the pair a bit...
    pair<int, int> objectPair;
    objectPair.first = num;         // num == object id (parameter)
    if (lineTokens.size() == 3)     // is it immovable?
        objectPair.second = statePair.second;       // yes, it's immovable

    
    auto room2ObjIt = room2Obj.find(statePair.first);

    // Did we find the room already?
    if (room2ObjIt == room2Obj.end())
    {
        // Nope, so let's add it now
        vector<pair<int, int>> objectList;

        // And add the pair to the list...
        objectList.push_back(objectPair);
        
        // ... and add the room to list
        room2Obj.insert(std::make_pair(statePair.first, objectList));
    }
    else
    {
        room2ObjIt->second.push_back(objectPair);
    }
}


void ObjectLocationSection::dumpDB()
{
    cout << "Dumping DB for ObjectLocationSection" << endl;
    for_each(obj2Room.begin(), obj2Room.end(),
             [](std::map<int, pair<int,int>>::value_type& kv)
    {
        cout << "Key: " << kv.first << "\nValue: " << kv.second.first << "/" << kv.second.second << endl;
    });

    cout << "Finished dumping DB for ObjectLocationSection" << endl;
}

tuple<bool, vector<pair<int, int>>> ObjectLocationSection::getInitialObjectsForRoom(const int& roomNum)
{
	// What we'll be returning, no matter what
	tuple<bool, vector<pair<int, int>>> objects;

	// Are there any objects for the given room?
	auto it = room2Obj.find(roomNum);

	if (it == room2Obj.end())
	{
		// Nope, no objects, so just set the boolean to false
		get<0>(objects) = false;
	}
	else
	{
		// Ah, there *are* some initial objects, so let's set
		// all that up now
		get<0>(objects) = true;
		get<1>(objects) = it->second;
	}

	return objects;
}