//
//  Section.h
//  Advent
//
//  Created by Ron Olson on 9/16/14.
//  Copyright (c) 2014 Ron Olson. All rights reserved.
//

#ifndef __Advent__Section__
#define __Advent__Section__

#include "Utility.h"

#include <string>
#include <vector>
#include <sstream>
#include <cstdint>
#include <cassert>
using namespace std;

// This is a base class for handling the various sections of the database.
// The subclasses will implement addLine() and dumpDB() in the way that makes
// sense to that particular section.

class Section
{
public:
    Section() = default;

    // For adding the data from the file in the section-specific way...
    virtual void addLine(int num, const vector<string>& lineTokens) = 0;

    // Debugging
    virtual void dumpDB() = 0;

    // Used in the game in an assert to verify database size
    virtual unsigned long getDBSize() = 0;

    virtual ~Section()
    {
    }
protected:

    // This is a special method in that we will build a string from the
    // vector of tokens, *excluding* the first token. Why? Because the first
    // item will be id field, which we don't want to handle here
    string assembleLine(const vector<string>& lineTokens)
    {
        stringstream line;
        bool firstPart = true;
        for_each(lineTokens.begin(), lineTokens.end(),
                 [&](const string& str)
        {
            if (firstPart == true)
            {
                firstPart = false;
            }
            else
            {
                line << trim(str) << ' ';
            }
        });

        return line.str();
    }
};

#endif /* defined(__Advent__Section__) */
